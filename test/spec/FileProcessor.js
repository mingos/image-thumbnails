describe("FileProcessor", function() {
	/*
	 * Mock out the FileReader API before each test, then restore the original value afterwards.
	 * This is needed because the FileReader is a global object and mocking it out would
	 * be persistent between tests.
	 */
	var fr;
	beforeEach(function() {
		fr = window.FileReader;
		window.FileReader = function() {
			this.readAsDataURL = function() {
				this.onload instanceof Function && this.onload({
					target: {
						result: "data:image/jpeg;charset=UTF-8,lumberjack"
					}
				});
			};
		};
	});
	afterEach(function() {
		window.FileReader = fr;
	});

	it("should reject non-images", function() {
		var fileProcessor = new Mingos.ImageThumbnails.FileProcessor();
		expect(fileProcessor.process({ type: "text/html" })).toBeFalsy();
		expect(fileProcessor.process({ type: "application/json" })).toBeFalsy();
		expect(fileProcessor.process({ type: "audio/mpeg" })).toBeFalsy();
		expect(fileProcessor.process({ type: "video/mpeg" })).toBeFalsy();
	});

	it("should process images and return their data URL", function() {
		var fileProcessor = new Mingos.ImageThumbnails.FileProcessor();
		var spy = jasmine.createSpy("spy");

		fileProcessor
			.process({ type: "image/jpeg" })
			.then(spy);
		waits(10);
		runs(function() {
			expect(spy).toHaveBeenCalledWith("data:image/jpeg;charset=UTF-8,lumberjack");
		});
	});
});
