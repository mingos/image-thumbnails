describe("Thumbnailer", function() {
	it("should create new images with correct src", function() {
		var thumbnailer = new Mingos.ImageThumbnails.Thumbnailer();

		var img = thumbnailer.makeImg("data:image/jpeg;base64,lumberjack");

		expect(img instanceof HTMLImageElement).toBeTruthy();
		expect(img.src).toBe("data:image/jpeg;base64,lumberjack");
	});

	it("should generate new thumbnail data URLs with correct size", function() {
		var exampleImageURL = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAAQABADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDwjW/ijFo6aXFof2i4kiiWeET3EU5KZPl+bteTY6IwBTKEHaSFVUQWpx4U8aHS7h9Ivpr/AFZbv/QrEmGWW6lE0oLShmZIyHnwgj2AGJpyQQrcf4y+HniiO+03T4UsZr5lnnFtZwPJdF0lWMuB5YkEbbAwVuNyygCMEgdbYXD+EfBWqzX1rn7ffNbzPZJE8chaHf8AZx+5MMKmJsEqj8NGdqBK/EZ0cPShCphnebb2b63X3f1sf3NKpUrxnGpH3Vs+6+++tn17H//Z';

		var thumbnailer = new Mingos.ImageThumbnails.Thumbnailer();
		var thumbnailURL;

		var img = new Image();

		runs(function() {
			thumbnailer.getThumbnailDataURL(exampleImageURL, 160, 120).then(function(url) {
				thumbnailURL = url;
			});
		});
		waits(10);
		runs(function() {
			img.src = thumbnailURL;
		});
		waits(10);
		runs(function() {
			expect(img.width).toBe(160);
			expect(img.height).toBe(120);
		});
	});
});
