module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON("package.json"),
		concat: {
			dev: {
				options: {
					process: function(src, filepath) {
						return src.replace(/\s*['"]use strict['"];\n/g, "");
					},
					banner: "/*! Image Thumbnails <%= pkg.version %>\n" +
						" *  Copyright (c) 2014 Dominik Marczuk\n" +
						" *  Licence: BSD-3-Clause: http://opensource.org/licenses/BSD-3-Clause\n" +
						" */\n" +
						"(function(window) {\n" +
						"\"use strict\";\n\n",
					footer: "})(window);\n"
				},
				files: {
					"app/js/script.js": [
						"app/scripts/ImageThumbnails.js",
						"app/scripts/Thumbnailer.js",
						"app/scripts/FileProcessor.js",
						"app/scripts/DropTarget.js"
					]
				}
			},
			modernizr: {
				options: {

				}
			}
		},
		less: {
			dev: {
				options: {
					cleancss: true
				},
				files: {
					"app/css/style.css": [
						"app/less/style.less"
					]
				}
			}
		},
		uglify: {
			dist: {
				options: {
					mangle: true,
					compress: {
						drop_console: true
					},
					banner: "/*! Image Thumbnails <%= pkg.version %>\n" +
						" *  Copyright (c) 2014 Dominik Marczuk\n" +
						" *  Licence: BSD-3-Clause: http://opensource.org/licenses/BSD-3-Clause\n" +
						" */\n"
				},
				files: {
					"dist/js/script.js": [
						"app/js/script.js"
					]
				}
			}
		},
		copy: {
			dist: {
				files: [
					{ expand: true, cwd: "app/css/", src: ["style.css"], dest: "dist/css/" },
					{ expand: true, cwd: "app/", src: ["index.html"], dest: "dist/" }
				]
			}
		},
		connect: {
			dev: {
				options: {
					port: 8080,
					base: "app",
					keepalive: true
				}
			},
			dist: {
				options: {
					port: 8081,
					base: "dist",
					keepalive: true
				}
			}
		},
		watch: {
			scripts: {
				files: ["app/scripts/**/*.js"],
				tasks: ["jshint:dev", "concat:dev", "karma:simple"]
			},
			tests: {
				files: ["test/spec/**/*.js"],
				tasks: ["karma:simple"]
			},
			styles: {
				files: ["app/less/**/*.less"],
				tasks: ["less:dev"]
			}
		},
		karma: {
			simple: {
				configFile: "karma.conf.js",
				singleRun: true,
				browsers: ["PhantomJS"],
				logLevel: "ERROR",
				reporters: ["dots"]
			},
			full: {
				configFile: "karma.conf.js",
				singleRun: true
			}
		},
		replace: {
			version: {
				options: {
					patterns: [
						{
							match: /VERSION: ".+"/,
							replacement: 'VERSION: "<%= pkg.version %>"'
						}
					]
				},
				files: [{
					expand: true,
					flatten: true,
					src: ["app/scripts/ImageThumbnails.js"],
					dest: "app/scripts/"
				}]
			}
		},
		jshint: {
			dev: {
				options: {
					browser: true,
					camelcase: true,
					curly: true,
					eqeqeq: true,
					es3: true,
					forin: true,
					freeze: true,
					immed: true,
					indent: 4,
					latedef: true,
					maxlen: 120,
					newcap: true,
					noarg: true,
					noempty: true,
					nomen: true,
					nonbsp: false,
					nonew: true,
					plusplus: false,
					quotmark: true,
					undef: true,
					strict: false,
					trailing: true,
					unused: true,
					globals: {
						Mingos: true
					}
				},
				files: {
					src: ["app/scripts/**/*.js"]
				}
			}
		}
	});

	// Load tasks
	grunt.loadNpmTasks("grunt-contrib-concat");
	grunt.loadNpmTasks("grunt-contrib-connect");
	grunt.loadNpmTasks("grunt-contrib-copy");
	grunt.loadNpmTasks("grunt-contrib-jshint");
	grunt.loadNpmTasks("grunt-contrib-less");
	grunt.loadNpmTasks("grunt-contrib-uglify");
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.loadNpmTasks("grunt-karma");
	grunt.loadNpmTasks("grunt-replace");

	// Custom tasks
	grunt.registerTask("bower", "install bower dev dependencies", function() {
		var exec = require("child_process").exec,
		    cb = this.async();
		exec("bower install", {}, function(err, stdout, stderr) {
			console.log(stdout);
			cb();
		});
	});
	grunt.registerTask("default", []);
	grunt.registerTask("dev", ["concat:dev", "less:dev"]);
	grunt.registerTask("dist", ["replace:version", "dev", "uglify:dist", "copy:dist"]);
	grunt.registerTask("test", ["jshint:dev", "bower", "karma:full"]);
	grunt.registerTask("serve", ["dist", "connect:dist"]);
};
