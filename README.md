# Image Thumbnails

A simple JavaScript demo application. Upload images using drag and drop, create thumbnails and preview full size images.

## How to run the project

Clone the project repository:

```
git clone https://bitbucket.org/mingos/image-thumbnails.git
```

`cd` to the project directory. Install dependencies:

```
npm install
```

Build the project and run a static file webserver:

```
grunt serve
```

Then, navigate to the URL the webserver will handle: `http://localhost:8081`.

## Running tests

The unit tests are run using Karma test runner:

```
grunt test
```
