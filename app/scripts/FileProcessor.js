/**
 * File processor class used for extracting the data URL.
 * @constructor
 */
var FileProcessor = function()
{
};

/**
 * Process a file. Only accepts images.
 * @param   {File}     file
 * @returns {Promise}       Promise that resolves with the image's data URL
 */
FileProcessor.prototype.process = function(file)
{
	"use strict";

	// reject non-images
	if(!file.type.match(/image/)) {
		return false;
	}

	return new Promise(function(resolve) {
		// create an instance of the FileReader and use it
		var fileReader = new FileReader();

		fileReader.onload = function(event) {
			resolve(event.target.result);
		};
		fileReader.readAsDataURL(file);
	});
};

window.Mingos.ImageThumbnails.FileProcessor = FileProcessor;
