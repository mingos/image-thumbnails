/**
 * Object that adds image thumbnails to a pool.
 * @constructor
 */
var Thumbnailer = function()
{
};

/**
 * Create a new img element containing the thumbnail
 * @param {String} url Data URL
 */
Thumbnailer.prototype.makeImg = function(url)
{
	"use strict";

	var img = document.createElement("IMG");
	img.src = url;

	return img;
};

/**
 * Scale the image proportionally and crop it
 * @param   {HTMLCanvasElement} canvas
 * @param   {HTMLImageElement}  image
 * @returns {String}                   Scaled and cropped data URL
 */
Thumbnailer.prototype.scaleAndCrop = function(canvas, image)
{
	"use strict";

	// image to canvas side width proportions, source image coords and sizes.
	var proportionW = image.width / canvas.width,
		proportionH = image.height / canvas.height,
		sx,
		sy,
		sw,
		sh;

	// processing is slightly different depending on whether the width or the height
	// is larger after scaling, so let's define two possible paths:
	if (proportionW > proportionH) {
		sx = Math.floor((image.width - (canvas.width * proportionH)) / 2);
		sw = image.width - sx * 2;
		sy = 0;
		sh = image.height;
	} else {
		sx = 0;
		sw = image.width;
		sy = Math.floor((image.height - (canvas.height * proportionW)) / 2);
		sh = image.height - sy * 2;
	}

	// the dimensions have been found; now let's do the cropping!
	var context = canvas.getContext("2d");
	context.drawImage(
		image,
		sx,
		sy,
		sw,
		sh,
		0,
		0,
		canvas.width,
		canvas.height
	);

	// the canvas is ready; return the cropped image as a data URL
	return canvas.toDataURL("image/jpeg");
};

/**
 * Create a new thumbnail
 * @param   {String}  url    Data URL
 * @param   {Number}  width  Thumbnail width
 * @param   {Number}  height Thumbnail height
 * @returns {Promise}        Promise that will resolve to the data URL
 */
Thumbnailer.prototype.getThumbnailDataURL = function(url, width, height)
{
	"use strict";

	var me = this;

	return new Promise(function(resolve) {
		var canvas = document.createElement("CANVAS");
		canvas.width = width;
		canvas.height = height;

		var image = new Image();
		image.onload = function() {
			resolve(me.scaleAndCrop(canvas, image));
		};
		image.src = url;
	});
};

window.Mingos.ImageThumbnails.Thumbnailer = Thumbnailer;
