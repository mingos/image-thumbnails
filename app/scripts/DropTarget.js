/**
 * The implementation of a drop target
 *
 * @param {HTMLElement}   element         The element that will become a drop zone
 * @param {HTMLElement}   imgContainer    The element containing clickable thumbnails
 * @param {FileProcessor} fileProcessor   An instance of the file processor for reading image data
 * @param {Thumbnailer}   thumbnailer     An instance of the thumbnailer to create img nodes
 * @param {Object}        userOptions     Options overriding the defaults
 * @constructor
 */
window.Mingos.ImageThumbnails.DropTarget = function(
	element,
	imgContainer,
	fileProcessor,
	thumbnailer,
	userOptions
) {
	"use strict";

	var i,
		options = {
			thumbnailWidth: 150,
			thumbnailHeight: 150,
			dragClass: "drag",
			closeClass: "close",
			closeContent: "×"
		};
	userOptions = userOptions || {};
	for (i in userOptions) {
		if (userOptions.hasOwnProperty(i)) {
			options[i] = userOptions[i];
		}
	}

	// react to the dragover event; mark the drop effect as copy and add class
	element.addEventListener("dragover", function(event) {
		event.stopPropagation();
		event.preventDefault();

		// mark the drop effect as copy
		event.dataTransfer.dropEffect = "copy";

		// add class; it will be removed on drag leave
		element.classList.add(options.dragClass);
	}, false);

	// react to drag leave to remove class.
	element.addEventListener("dragleave", function() {
		element.classList.remove(options.dragClass);
	}, false);

	// react to the drop event; do the file transfer and processing
	element.addEventListener("drop", function(event) {
		event.stopPropagation();
		event.preventDefault();

		/**
		 * List of dropped files
		 * @type {FileList}
		 */
		var files = event.dataTransfer.files;

		handleFiles(files);

		// also remove the drag class (no dragleave will be fired here)
		element.classList.remove(options.dragClass);
	}, false);

	// create an invisible file input for handling file uploads.
	// Clicking on the drop zone will enable using the file input.
	var input = document.createElement("INPUT");
	input.type = "file";
	input.multiple = "multiple";
	element.addEventListener("click", function() {
		input.click();
	});
	input.addEventListener("change", function() {
		if (input.files.length) {
			handleFiles(input.files);
		}
	});

	/**
	 * Handle a FileList - process all files within it
	 * @param {FileList} files
	 */
	var handleFiles = function(files)
	{
		// iterate over the dropped files and execute the file processing on them
		for (var i = 0; ; ++i) {
			var f = files[i];
			if (!f) {
				break;
			}
			fileProcessor.process(f).then(handleURL);
		}
	};

	/**
	 * Handle a data URL
	 * @param {String} url Data URL of the originally uploaded image
	 */
	var handleURL = function(url)
	{
		thumbnailer.getThumbnailDataURL(
			url,
			options.thumbnailWidth,
			options.thumbnailHeight
		).then(function(thumbURL) {
			var img = thumbnailer.makeImg(thumbURL);

			// make link
			var a = document.createElement("A");
			a.href = url;
			a.target = "_blank";
			a.appendChild(img);

			// make close button
			var close = document.createElement("SPAN");
			close.classList.add(options.closeClass);
			close.innerText = options.closeContent;
			a.appendChild(close);

			// close button should react to a click
			close.addEventListener("click", function(event) {
				event.stopPropagation();
				event.preventDefault();

				var a = event.target.parentNode;
				a.parentNode.removeChild(a);
			}, false);

			// append the link with the thumbnail image to the specified target container
			imgContainer.appendChild(a);
		});
	};
};
